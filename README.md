# BLOGPOSTER: A free (as in freedom) blog system

<!-- Presentation -->
**Author**: Adrian Estevez

**Name**: blogposter

**Description**: The blogposter script allows a user to create and manipulate a static webpage

## Features
Allowed actions:
- Upload image(s)
- Create blog draft
- Publish blog draft or uploaded image
- Delete published posts
- Hide published posts

<!-- Usage -->
Usage: blogposter [OPTION] [FILE|POSTID]

Options:
```sh
-c       Creates a blog draft
-d       Deletes post, needs ID
-h       Hide post, needs ID
--help   Prints this message
--init   Initializes a webpage from scratch
-p       Publish post, needs ID (updates date)
-T       Apply templates to all posts
-u       Uploads and image, doesnt publish
-U       Updates a publication content, needs ID (does not update date)
```

<!-- Installation -->
## Installation
+ Clone this repository:
```sh
git clone https://gitlab.com/adrianstvz/blogpersonal.git .
```
+ Initialize the webpage:
```sh
blogposter --init
```
+ Learn about the _markers_

### MARKERS
In order to generate all the html pages of the webpage, the script make use of some markers placed inside each page.
This markers are defined as follow: 
```
<!-- # CODE # --> 
```
Codes:
+ M: Marks the end of the mandatory part, which always gets updated
+ BC _Begin Content_: Marks the beggining of the content, used to 
+ EC _End Content_: Marks the end of the content, used to add a footer
+ SB/SE _Sh**post Begins/Ends_: Used to mark were individual posts are inserted

